package com.example.databaseapp;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class User {
    @PrimaryKey
    private int uid;
    private String name;
    @ColumnInfo(name = "last_name")
    private String lastName;
    // getters and setters are ignored for brevity but they are required for Room to work.

    public User(){}
    public int getUid(){return this.uid;}
    public String getName(){return this.name;}
    public String getLastName(){return this.lastName;}

    public void setUid(int uid){ this.uid = uid;}
    public void setName(String name){ this.name = name;}
    public void setLastName(String lastName){ this.lastName = lastName;}
}