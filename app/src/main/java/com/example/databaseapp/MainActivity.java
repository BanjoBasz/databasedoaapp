package com.example.databaseapp;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppDatabase db = Room
            .databaseBuilder(getApplicationContext(), AppDatabase.class, "database-name")
            .allowMainThreadQueries() // SHOULD NOT BE USED IN PRODUCTION !!!
            .build();

        User u = new User();
        u.setUid(1113);
        u.setLastName("Rijsdijk");
        u.setName("Jeroen");
       // db.userDao().insertAll(u);
        List<User> userList = db.userDao().loadAll();
        Log.d("tag",userList.get(0).getLastName().toString());

    }
}
